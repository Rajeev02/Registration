package com.example.rajeev.registration;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by rajeev on 1/7/16.
 */
public class MyDb extends SQLiteOpenHelper{
    public static final String DB_NAME = "Registration";
    public static final int VERSION = 1;

    public MyDb(Context context)
    {
        super(context, DB_NAME, null, VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try{
            String qry = "create table users(u_name TEXT, u_email TEXT PRIMARY KEY, u_phone TEXT, u_address TEXT, u_hobbies TEXT)";
            sqLiteDatabase.execSQL(qry);
        }
        catch (Exception ex)
        {
            Log.e("User Table Creation",""+ex);
        }

    }

    public boolean insertUserData (String name, String email, String phone, String address, String hobbies)
    {
        try
        {
            String qry1 = "insert into users values("+"'"+name+"','"+email+"','"+phone+"','"+address+"','"+hobbies+"'"+")";
            SQLiteDatabase sqLiteDatabase=getWritableDatabase();
            sqLiteDatabase.execSQL(qry1);
            return true;
        }
        catch (Exception ex)
        {
            Log.e("Insertion Exception",""+ex);
            return false;
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
