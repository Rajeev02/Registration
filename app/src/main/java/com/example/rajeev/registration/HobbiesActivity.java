package com.example.rajeev.registration;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

public class HobbiesActivity extends AppCompatActivity {
CheckBox[] cb=new CheckBox[40];

    static int m =0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hobbies);
        cb[0]=(CheckBox)findViewById(R.id.checkBox);
        cb[1]=(CheckBox)findViewById(R.id.checkBox2);
        cb[2]=(CheckBox)findViewById(R.id.checkBox3);
        cb[3]=(CheckBox)findViewById(R.id.checkBox4);
        cb[4]=(CheckBox)findViewById(R.id.checkBox5);
        cb[5]=(CheckBox)findViewById(R.id.checkBox6);
        cb[6]=(CheckBox)findViewById(R.id.checkBox7);
        cb[7]=(CheckBox)findViewById(R.id.checkBox8);
        cb[8]=(CheckBox)findViewById(R.id.checkBox9);
        cb[9]=(CheckBox)findViewById(R.id.checkBox10);
        cb[10]=(CheckBox)findViewById(R.id.checkBox11);
        cb[11]=(CheckBox)findViewById(R.id.checkBox12);
        cb[12]=(CheckBox)findViewById(R.id.checkBox13);
        cb[13]=(CheckBox)findViewById(R.id.checkBox14);
        cb[14]=(CheckBox)findViewById(R.id.checkBox15);
        cb[15]=(CheckBox)findViewById(R.id.checkBox16);
        cb[16]=(CheckBox)findViewById(R.id.checkBox17);
        cb[17]=(CheckBox)findViewById(R.id.checkBox18);
        cb[18]=(CheckBox)findViewById(R.id.checkBox19);
        cb[19]=(CheckBox)findViewById(R.id.checkBox20);
        cb[20]=(CheckBox)findViewById(R.id.checkBox21);
        cb[21]=(CheckBox)findViewById(R.id.checkBox22);
        cb[22]=(CheckBox)findViewById(R.id.checkBox23);
        cb[23]=(CheckBox)findViewById(R.id.checkBox24);
        cb[24]=(CheckBox)findViewById(R.id.checkBox25);
        cb[25]=(CheckBox)findViewById(R.id.checkBox26);
        cb[26]=(CheckBox)findViewById(R.id.checkBox27);
        cb[27]=(CheckBox)findViewById(R.id.checkBox28);
        cb[28]=(CheckBox)findViewById(R.id.checkBox29);
        cb[29]=(CheckBox)findViewById(R.id.checkBox30);
        cb[30]=(CheckBox)findViewById(R.id.checkBox31);
        cb[31]=(CheckBox)findViewById(R.id.checkBox32);
        cb[32]=(CheckBox)findViewById(R.id.checkBox33);
        cb[33]=(CheckBox)findViewById(R.id.checkBox34);
        cb[34]=(CheckBox)findViewById(R.id.checkBox35);
        cb[35]=(CheckBox)findViewById(R.id.checkBox36);
        cb[36]=(CheckBox)findViewById(R.id.checkBox37);
        cb[37]=(CheckBox)findViewById(R.id.checkBox38);
        cb[38]=(CheckBox)findViewById(R.id.checkBox39);
        cb[39]=(CheckBox)findViewById(R.id.checkBox40);

          }
    public void hobbiesButton(View v)
    {
        Intent ref=getIntent();
        Bundle b=ref.getExtras();
        final String name=b.getString("k1");
        final String email=b.getString("k2");
        final String phone=b.getString("k3");
        final String address=b.getString("k4");
        String S="";
        for(int i=0;i<40;i++)
        {
            if(cb[i].isChecked())
            {     String ls;

                ls=cb[i].getText().toString().trim();
                S=S+ls+", ";
            }
        }

        try
        {
            boolean res = MainActivity.mdb.insertUserData(name, email, phone, address, S);

            if(res)
            {
                Toast.makeText(HobbiesActivity.this, "Registration Successful", Toast.LENGTH_SHORT).show();

                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:" + ""+email));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Dear "+name+" Your Registration Information");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "Your Hobbies are "+S);


                try {


                    startActivity(new Intent(this,MainActivity.class));

                    startActivity(Intent.createChooser(emailIntent, "Send email using..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(HobbiesActivity.this, "No email clients installed.", Toast.LENGTH_SHORT).show();
                }
            }


        }
        catch (Exception ex)
        {
            Log.e("Data Insertion",""+ex);
            Toast.makeText(HobbiesActivity.this, "Data not insert", Toast.LENGTH_SHORT).show();
        }


        }
    }


