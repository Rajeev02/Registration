package com.example.rajeev.registration;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegistrationPag extends AppCompatActivity {

    EditText et1,et2,et3,et4;
    Button b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_pag);

        et1=(EditText)findViewById(R.id.editText);
        et2=(EditText)findViewById(R.id.editText2);
        et3=(EditText)findViewById(R.id.editText3);
        et4=(EditText)findViewById(R.id.editText4);
        b=(Button)findViewById(R.id.button3);
    }
    public void registrationButton(View v)
    {
        boolean o = true;
        try {
            String name = et1.getText().toString().trim();
            String email = et2.getText().toString().trim();
            String phone = et3.getText().toString().trim();
            String address = et4.getText().toString().trim();

                   if(!Patterns.PHONE.matcher(phone).matches()||phone.length()<10)
                   {
                       Toast.makeText(RegistrationPag.this, "Enter 10 digit phone number", Toast.LENGTH_SHORT).show();
                       o=false;
                   }

                    if(!Patterns.EMAIL_ADDRESS.matcher(email).matches())
                    {
                        Toast.makeText(RegistrationPag.this, "Please Check Email id", Toast.LENGTH_SHORT).show();
                        o=false;
                    }
            if(name.isEmpty()||name.length()<5)
            {
                Toast.makeText(RegistrationPag.this, "Please fill the Name more than 5 character", Toast.LENGTH_SHORT).show();
            }
            else if (email.isEmpty()) {
                Toast.makeText(RegistrationPag.this, "Please fill the Email", Toast.LENGTH_SHORT).show();
            }
            else if (phone.isEmpty())
            {
                Toast.makeText(RegistrationPag.this, "Please fill the Phone Number", Toast.LENGTH_SHORT).show();
            }
            else if(address.isEmpty()||address.length()<15)
            {
                Toast.makeText(RegistrationPag.this, "Please Fill the address more than 15 character", Toast.LENGTH_SHORT).show();
            }

            else {


                if (o == true) {

                    Intent i = new Intent(this, HobbiesActivity.class);
                    i.putExtra("k1", name);
                    i.putExtra("k2", email);
                    i.putExtra("k3", phone);
                    i.putExtra("k4", address);

                    et1.setText("");
                    et2.setText("");
                    et3.setText("");
                    et4.setText("");


                    startActivity(i);
                    finish();
                }

            }
        }
        catch (Exception ex)
        {
            Log.e("Invalid",""+ex);
            Toast.makeText(RegistrationPag.this, "Invalid Input", Toast.LENGTH_SHORT).show();
        }
    }
}
