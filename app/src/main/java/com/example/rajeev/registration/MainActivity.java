package com.example.rajeev.registration;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    static MyDb mdb;
    LinearLayout ll;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mdb=new MyDb(this);
        ll=(LinearLayout)findViewById(R.id.ll1);

        try {
            SQLiteDatabase db = MainActivity.mdb.getWritableDatabase();
            String qry = "select * from users";

            Cursor cursor = db.rawQuery(qry,null);
            cursor.moveToFirst();
            do {
                String name = cursor.getString(0);
                String email = cursor.getString(1);
                String hobbies = cursor.getString(4);


                TextView tv = new TextView(this);
                tv.setText( "Name    : " + name + "\n"+"Email    : " + email + "\n"+"Hobbies : " + hobbies + "\n");
                tv.setTextSize(16);
                tv.setTextColor(Color.GRAY);
                ll.addView(tv);

            }while(cursor.moveToNext());

            }
        catch (Exception ex)
        {
            Log.e("Read Data",""+ex);
            Toast.makeText(MainActivity.this, "Reading Problem", Toast.LENGTH_SHORT).show();
        }


    }



    public void addMemberButton(View v)
    {
           startActivity(new Intent(this,RegistrationPag.class));
        finish();
    }
    public void deleteMemberButton(View v)
    {
        Toast.makeText(MainActivity.this, "Under development", Toast.LENGTH_SHORT).show();
    }
}
